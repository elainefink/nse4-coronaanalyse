#include "pch.h"
#include "CppUnitTest.h"
#include "Corona2.h"
#include "../MGIN-Lab7/MGIN-Lab7.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace UnitTest1
{

	TEST_CLASS(UnitTest1)
	{
	public:
		TEST_METHOD(TestFunction)
		{
			Assert::IsFalse(false,0);
		}

		TEST_METHOD(IsNotEmpty)
		{
			Corona corona;
			int result = corona.NotNull(17,68,true);
			Assert::AreEqual(1,result);
		}

		TEST_METHOD(ResultArrived) 
		{
			Corona corona; 
			bool check = corona.Result();

			Assert::AreEqual(true, check);
		}

	};
}
